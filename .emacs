;; first of all - get rid of toolbars
(tool-bar-mode -1)
(menu-bar-mode -1)

(require 'package)
(add-to-list 'package-archives
             '("melpa" . "http://melpa.org/packages/"))
(package-initialize)

(unless (package-installed-p 'use-package)
  (progn
    (package-refresh-contents)
    (package-install 'use-package)))

(require 'use-package)

(use-package hemisu-theme :ensure t)
(load-theme 'hemisu-light t)

(use-package js3-mode :ensure t :mode ("\\.js$" . js3-mode) :mode("\\.json$" . js3-mode))
(use-package js2-mode :ensure t :commands js2-mode)
(use-package scala-mode :ensure t :mode ("\\.scala$" . scala-mode))
(use-package autopair :ensure t)
(use-package web-mode :ensure t :mode ("\\.html?$" . web-mode))
(use-package company :ensure t :defer t)
(use-package company-tern :ensure t :defer t)
(use-package company-web :ensure t :defer t)
(use-package ido-vertical-mode :ensure t :defer t)
(use-package magit :ensure t :defer t)
(use-package nvm :ensure t :defer 1)

(require 'company)
(add-hook 'after-init-hook 'global-company-mode)
(add-to-list 'company-backends 'company-tern)
(add-to-list 'company-backends 'company-web-html)

(setq-default indent-tabs-mode nil)

;; Tern
(add-to-list 'load-path "/usr/local/lib/node_modules/tern/emacs/")
(autoload 'tern-mode "tern.el" nil t)
(add-hook 'js2-mode-hook (lambda () (tern-mode t)))
(add-hook 'js3-mode-hook (lambda () (tern-mode t)))

(fset 'yes-or-no-p 'y-or-n-p)
(show-paren-mode 1)
(electric-indent-mode 1)
(electric-pair-mode 1)
(global-linum-mode 1)
(global-hl-line-mode 1)
(add-hook 'before-save-hook 'whitespace-cleanup)
(add-hook 'before-save-hook (lambda() (delete-trailing-whitespace)))
(setq backup-directory-alist '(("." . "~/.emacs.backup.d")))
(setq auto-save-file-name-transforms
          `((".*" ,"~/.emacs.backup.d" t)))

(setq ido-enable-flex-matching 1)
(setq ido-everywhere 1)
(ido-mode 1)
(ido-vertical-mode 1)

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(custom-safe-themes
   (quote
    ("77bd459212c0176bdf63c1904c4ba20fce015f730f0343776a1a14432de80990" "7feeed063855b06836e0262f77f5c6d3f415159a98a9676d549bfeb6c49637c4" "c1fb68aa00235766461c7e31ecfc759aa2dd905899ae6d95097061faeb72f9ee" default)))
 '(inhibit-startup-screen t)
 '(js2-basic-offset 2)
 '(js3-boring-indentation t)
 '(js3-enter-indents-newline t)
 '(js3-highlight-level 3)
 '(web-mode-code-indent-offset 2)
 '(web-mode-css-indent-offset 2)
 '(web-mode-enable-comment-keywords t)
 '(web-mode-markup-indent-offset 2))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(font-lock-constant-face ((t (:foreground "#538192" :weight bold))))
 '(font-lock-function-name-face ((t (:weight bold))))
 '(highlight ((t (:background "LightCyan1"))))
 '(js3-external-variable-face ((t (:foreground "DarkOrange3"))))
 '(web-mode-block-attr-name-face ((t (:foreground "#8fbc8f"))))
 '(web-mode-html-attr-equal-face ((t (:inherit font-lock-default-face))))
 '(web-mode-html-attr-name-face ((t (:inherit font-lock-constant-face :weight extra-bold))))
 '(web-mode-html-tag-face ((t (:inherit font-lock-function-name-face)))))

(defun run-term (&optional arg)
  (interactive "P")
  (let ((default-directory default-directory))
    (when arg
      (cond ((bound-and-true-p cppcm-build-dir)
             (cd cppcm-build-dir))
            ((vc-root-dir)
             (cd (vc-root-dir)))
            (t (when (string-match "^.*/src/$" default-directory)
                 (cd "../")
                 (when (file-directory-p "build")
                   (cd "build"))))))
    (if (window-system)
        (start-process "my-urxvt" nil "x-terminal-emulator")
        (start-process "my-tmux" nil
                       "tmux" "split-window" "-h"))))

(global-set-key (kbd "C-c x") 'run-term)
(global-set-key (kbd "M-o") 'run-term)

(add-to-list 'default-frame-alist '(font . "DejaVu Sans Mono-10"))
